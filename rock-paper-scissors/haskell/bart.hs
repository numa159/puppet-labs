--La buena piedra, nada le gana
import Control.Monad (forever)
import System.IO

main = do
  hSetBuffering stdin LineBuffering
  hSetBuffering stdout LineBuffering

  forever (esperarTurno >> jugarPiedra)
    where jugarPiedra = putStrLn "rock"
          esperarTurno = getLine

