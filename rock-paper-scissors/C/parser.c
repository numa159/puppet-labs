#include "parser.h"

int serializeInput(const char* input, serialized_t** into) {
	char* token;
	char** array;
	size_t size = 1;
	char* inputCpy = strdup(input);
	
	array = (char**)malloc(sizeof(char*) * size);
	if(array == NULL) {
		perror("Unable to allocate enough memory");
		return 1;
	}
	token = strtok(inputCpy, "|");

	while(token) {
		array[size-1] = strdup(token);
		/* printf("Token Nº%zu: %s\n", size-1, token); */

		token = strtok(NULL, "|");
		if(token) {
			size++;
			array = (char**)realloc(array, sizeof(char*) * size);
			if(array == NULL) {
				perror("Unable to allocate enough memory");
				return 1;
			}
		}
	}

	free(inputCpy);

	(*into)->strings = array;
	(*into)->length = size;

	return 0;
}

int parseLine(serialized_t** into) {
	char* line;
	size_t bufferSize = 32;
	size_t characters = 0;

	line = (char *)malloc(bufferSize * sizeof(char));
	if(line == NULL) {
		perror("Unable to allocate enough memory");
		return 1;
	}

	characters = getline(&line, &bufferSize, stdin);
	if(characters <= 0) {
		perror("Error reading line");
		return 1;
	}

	if(serializeInput(line, into)) return 1;
	free(line);
	return 0;
}

void freeSerialized(serialized_t** toDestroy) {
	int i;
	for(i=0; i < (*toDestroy)->length; i++) {
		free((*toDestroy)->strings[i]);
	}
	free((*toDestroy)->strings);
	free(*toDestroy);
}
