#include "start.h"

int parseStartingStatement(char** myName) {
	serialized_t* startingStatement;
	startingStatement = (serialized_t*)malloc(sizeof(serialized_t));
	if(startingStatement == NULL) {
		perror("Unable to allocate enough memory");
		return 1;
	}
	parseLine(&startingStatement);

	if(
		strcmp(startingStatement->strings[0], "start") != 0 ||
		startingStatement->length != 4
	) {
		perror("Invalid starting statement");
		return 1;
	}

	srand(atoi(startingStatement->strings[3]));

	*myName = strdup(startingStatement->strings[1]);

	freeSerialized(&startingStatement);
	startingStatement=NULL;

	return 0;
}

