#include "rps.h"

char* chooseRps(int roundNmb) {
	char* plays[3] = {"rock","paper","scissors"};

	/* Only order and discipline will be able to save us... */
	return plays[roundNmb % 3];
}

enum Result playRound(const char * myName, int roundNmb) {
	enum Result result;

	char* winner;
	char* chosen = chooseRps(roundNmb);
	printf("%s\n", chosen);

	serialized_t* roundStatement;
	roundStatement = (serialized_t*)malloc(sizeof(serialized_t));
	if(roundStatement == NULL) {
		perror("Unable to allocate enough memory");
		return 1;
	}
	parseLine(&roundStatement);


	if(roundStatement->length <= 1) {
		result = End;
	} else if(strcmp(roundStatement->strings[WINNER_IDX], WINNER_TAG) == 0) {
		winner = strdup(roundStatement->strings[WINNER_NAME_IDX]);
		winner[strlen(winner) - 1] = '\0';
		if(strcmp(winner, myName) == 0) {
			result = Won;
		} else {
			result = Lost;
		}
		free(winner);
	} else {
		result = Tied;
	}

	freeSerialized(&roundStatement);
	roundStatement=NULL;

	return result;
}

int rockPaperScissors(void) {
	char* myName;
	int i = 0;

	if(parseStartingStatement(&myName)) return 1;
	while(playRound(myName, i) < End) {
		i++;
	};
	free(myName);
	return 0;
}
