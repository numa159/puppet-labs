from random import choice 

## Format

# start|<puppet_id>|<seed>
# update|<puppet_1>:<accion_puppet_1>|<puppet_2>:<accion_puppet_2>|<resultado>|<ganador></ganador>

mi_nombre = None
jugada_segura = None

def elegir_al_azar():
  return choice(["rock", "paper", "scissors"])

while True:
  data = input().strip().split("|")
  if data[0] == "start":
    mi_nombre = data[1]
    jugada_segura = elegir_al_azar()
    print(jugada_segura, flush=True)
  else:
    hubo_ganador = data[3] == 'winner'
    if (hubo_ganador and data[4] == mi_nombre):
      print(jugada_segura, flush=True)
    else:
      jugada_segura = elegir_al_azar()
      print(jugada_segura, flush=True)