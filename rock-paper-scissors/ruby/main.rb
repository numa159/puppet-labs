$stdout.sync = true

# start|<puppet_id>|<seed>
# update|<puppet_1>:<accion_puppet_1>|<puppet_2>:<accion_puppet_2>|<resultado>|<ganador></ganador>

def elegir_al_azar
  ['rock', 'paper', 'scissors'].sample
end

jugada_segura = nil
mi_nombre = nil

while true
  input = gets
  data = input.strip.split('|')
  if data[0] == 'start'
    mi_nombre = data[1]
    jugada_segura = elegir_al_azar()
    puts jugada_segura
  else
    hubo_ganador = data[3] == 'winner'
    if hubo_ganador && data[4] == mi_nombre
      puts jugada_segura
    else
      jugada_segura = elegir_al_azar()
      puts jugada_segura
    end
  end
end